﻿unit uDateUtils;

interface
uses
  Windows, System.SysUtils;
type
  TNanoTime = Int64; // = FileTime (количество 100-наносекундных интервалов с 1/1/1601)

  TSysDate = record
    wYear: Word;
    wMonth: Word;
    wDayOfWeek: Word;
    wDay: Word;
  end;

  TSysTime = record
    wHour: Word;
    wMinute: Word;
    wSecond: Word;
    wMilliseconds: Word;
  end;

  TSysDateTime = record
    case Integer of
    0: (SystemTime: TSystemTime);
    1: (
      wYear: Word;
      wMonth: Word;
      wDayOfWeek: Word;
      wDay: Word;
      wHour: Word;
      wMinute: Word;
      wSecond: Word;
      wMilliseconds: Word);
    2: (
      Date: TSysDate;
      Time: TSysTime);
    3: (
      iDate: int64;
      iTime: int64);
  end;

  PSysDateTime = ^TSysDateTime;


function ISODateToDateTimeDef(ISODate:string; const Default: TDateTime): TDateTime;
function DateTimeToISOZ(DateTime: TDateTime):string;
function DateTimeToISO(DateTime: TDateTime):string;
function DateToISO(DateTime: TDateTime):string;
function wDayToISO(wYear, wMonth, wDay: word): string;
function TimeToISOZ(DateTime: TDateTime):string;

function SysTimeToIso(ASysTime: TSysTime): string;
function SysTimeToIsoZ(ASysTime: TSysTime): string;
function SysDateToIso(ASysDate: TSysDate): string;

function SysDateTimeToIso(st: PSysDateTime): string;
function SysDateTimeToIsoZ(st: PSysDateTime): string;

function GetNowStrISO(): String;

procedure GetSysLocalTime(st: PSysDateTime);

function rdtsc: int64; register;

function IntToSStr(i: integer): string; // Число с разделителем тысяч

implementation

const
  ISO8601DateTimeFormatZ = 'yyyy-mm-dd hh:nn:ss.zzz';
  ISO8601DateTimeFormat = 'yyyy-mm-dd hh:nn:ss';
  ISO8601DateFormat = 'yyyy-mm-dd';
  ISO8601TimeFormatZ = 'hh:nn:ss.zzz';
var
  formatSetting: TFormatSettings;

function ISODateToDateTimeDef(ISODate:string; const Default: TDateTime): TDateTime;
begin
  result := StrToDateTimeDef(ISODate, Default, formatSetting);
end;

function DateTimeToISOZ(DateTime: TDateTime):string;
begin
  DateTimeToString(Result, ISO8601DateTimeFormatZ, DateTime)
end;

function TimeToISOZ(DateTime: TDateTime):string;
begin
  DateTimeToString(Result, ISO8601TimeFormatZ, DateTime)
end;

function DateTimeToISO(DateTime: TDateTime):string;
begin
  DateTimeToString(Result, ISO8601DateTimeFormat, DateTime)
end;

function DateToISO(DateTime: TDateTime):string;
begin
  DateTimeToString(Result, ISO8601DateFormat, DateTime)
end;

function IntToSStr(i: integer): string;//Число с разделителем тысяч
var
  L: Integer;
begin
  Result := IntToStr(i);
  L := Length(Result);
  if I >= 1000 then // 1 234 567 890
  begin
    Insert(' ', Result, L - 2);
    if I >= 1000000 then
    begin
      Insert(' ', Result, L - 5);
      if I >= 1000000000 then
        Insert(' ',Result, L - 8)
    end;
  end;
end;

function SecStr(Sec: word): string;
begin
  Result := inttostr(Sec);
  if Sec < 10  then
    Result := '0' + Result;
end;

function mSecStr(mSec: word): string;
begin
  Result := inttostr(mSec);
  if Length(Result) < 2  then
    Result := '00' + Result;
  if Length(Result) < 3  then
    Result := '0' + Result;
end;

function wDayToISO(wYear, wMonth, wDay: word): string;
begin
  result := IntToStr(wYear) + '-' + SecStr(wMonth) + '-' + SecStr(wDay);
end;

function SysTimeToIso(ASysTime: TSysTime): string;
begin
  Result := SecStr(ASysTime.wHour) + ':' + SecStr(ASysTime.wMinute) + ':' + SecStr(ASysTime.wSecond)
end;

function SysTimeToIsoZ(ASysTime: TSysTime): string;
begin
  Result := SecStr(ASysTime.wHour) + ':' + SecStr(ASysTime.wMinute) + ':' + SecStr(ASysTime.wSecond)
    + '.' + mSecStr(ASysTime.wMilliseconds)
end;

function SysDateToIso(ASysDate: TSysDate): string;
begin
  Result := inttostr(ASysDate.wYear) + '-' + SecStr(ASysDate.wMonth) + '-' + SecStr(ASysDate.wDay)
end;

function SysDateTimeToIso(st: PSysDateTime): string;
begin
  Result := SysDateToIso(st.Date) + ' ' + SysTimeToIso(st.Time);
end;

function SysDateTimeToIsoZ(st: PSysDateTime): string;
begin
  Result := SysDateToIso(st.Date) + ' ' + SysTimeToIsoZ(st.Time);
end;

function rdtsc: int64; register;
asm
  db $0F; db $31;
end;


procedure GetSysLocalTime(st: PSysDateTime);
begin
  GetLocalTime(PSystemTime(st)^);
end;

function GetNowStrISO(): String;
var
  st: TSysDateTime;
begin
  GetLocalTime(st.SystemTime);
  Result := SysDateTimeToIso(@st);
//  ZStr(st.wDay, 2) + '.' + ZStr(st.wMonth, 2) + '.' + ZStr(st.wYear, 4) +
//    ' ' + ZStr(st.wHour, 2) + ':' + ZStr(st.wMinute, 2) + ':' + ZStr(st.wSecond, 2) +
//    '.' + ZStr(st.wMilliseconds, 3);
end;

initialization
  formatSetting.TimeSeparator := ':';
  formatSetting.ShortDateFormat := ISO8601DateFormat;
  formatSetting.DateSeparator := '-';

end.
