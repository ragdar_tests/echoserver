unit uConsoleLog;

interface
uses
  uLogIntf;

procedure LogInfo(const AMSG: string);
procedure LogError(const AMSG: string);

procedure SetLog(ALog: ILog);

implementation

var
  LocalLog: ILog;

procedure SetLog(ALog: ILog);
begin
  LocalLog := ALog;
end;


procedure LogInfo(const AMSG: string);
begin
  Writeln(AMSG);
  LocalLog.Log(lmInfo, AMSG);
end;

procedure LogError(const AMSG: string);
begin
  Writeln('Error. ' + AMSG);
  LocalLog.Log(lmError,AMSG);
end;


end.

