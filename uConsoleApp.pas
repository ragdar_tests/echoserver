unit uConsoleApp;

interface

uses
  Windows, SysUtils, messages, Classes, uConsoleThread,
  uLogIntf, uMQ, uServer;

type
  TConsoleApp = class(TInterfacedObject, IReceiver)
  private
    FServer: TServer;
    FLog: ILog;
    FMQ: IMQ;
    FConsoleThread: TConsoleThread;
    { IUnknown }
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
   { IReceiver }
    procedure ReceiveMessage(AMsg: Integer; AData: RawByteString);
    { IApp }
   //procedure SavePayment(APayment: IModemPay); // ��������� ������
    procedure LogInfo(const AMSG: string);
    procedure LogError(const AMSG: string);
    procedure init();
    procedure Run;
  public
    constructor Create(ALog: ILog);
    destructor Destroy; override;
    procedure Start;
    procedure ShowHelp;
    procedure ShowStartHelp;
    procedure Quit;
//    property AppPath: string read FAppPath write FAppPath;
//    property InterfaceList: TInterfaceList read FObjectList write FObjectList;
  end;

function AppAlreadyStarted(Log: ILog):Boolean;

implementation

function AppAlreadyStarted(Log: ILog):Boolean;
var
  err: Integer;
  Mutex: THandle;
begin
  Mutex := CreateMutex(nil, false, PWideChar(paramstr(0)));
  err := GetLastError;
  if err = ERROR_ALREADY_EXISTS then
    Log.Log(lmWarning, 'Application alredy started');
  if Mutex = 0 then
    Log.Log(lmError, 'Error CreateMutex:' + SysErrorMessage(err));
  Result := (Mutex = 0) or (err = ERROR_ALREADY_EXISTS);
end;

var
  MainThreadID: Cardinal;
  Event_Close: THandle;

function CtrlHandler(dwCtrlType: DWORD): DWORD; stdcall;
begin
  case dwCtrlType of
    CTRL_C_EVENT,            // User hit CTRL-C
    CTRL_BREAK_EVENT,        // User hit CTRL-BREAK
    CTRL_LOGOFF_EVENT,       // User log off his session
    CTRL_CLOSE_EVENT,        // Close signal
    CTRL_SHUTDOWN_EVENT :    // Window shutdown signal
    begin
      PostThreadMessage(MainThreadID, WM_QUIT, 0, 0);
      WaitForSingleObject(Event_Close, INFINITE);
      Result := 1;
    end;
  else
    Result := 0;
  end;
end;

{ TConsoleManager }
constructor TConsoleApp.Create(ALog: ILog);
begin
  inherited Create;
  FLog := ALog;

  Event_Close := CreateEvent(nil, True, False, nil);
//  FAppPath := ExtractFileDir(ParamStr(0));
//  FObjectList := TInterfaceList.create;
  FMQ := TMQ.Create;
  FMQ.Subscribe(Self);

  FConsoleThread := TConsoleThread.Create(true);
  FConsoleThread.SetMQ(FMQ);
//  FConsoleThread.FMainThread := GetCurrentThreadId;

  FormatSettings.DecimalSeparator := '.';
  MainThreadID := GetCurrentThreadId;

  FServer := TServer.Create(FLog);
  FServer.Port := 1500;
  if not SetConsoleCtrlHandler(@CtrlHandler, True) then
    LogError('Console handler is not installed');

end;

destructor TConsoleApp.Destroy;
begin
  FServer.Free;
//  FMQ.Unsubscribe(Self);
//  FreeAndNil(FObjectList);
  FConsoleThread.Free;
  CloseHandle(Event_Close);
  inherited;
end;

procedure TConsoleApp.Init;
begin
  FConsoleThread.Start;
  ShowStartHelp;
end;

procedure TConsoleApp.LogError(const AMSG: string);
begin
  Writeln('Error. ' + AMSG);
  FLog.Log(lmError, AMSG);
end;

procedure TConsoleApp.LogInfo(const AMSG: string);
begin
  Writeln(AMSG);
  FLog.Log(lmInfo, AMSG)
end;

procedure ClearConsoleWindow;
var
  ConsoleHandle:THandle;
  ConsoleInfo: TConsoleScreenBufferInfo;
  Coord: TCoord;
  WrittenChars: DWORD;
begin
  FillChar(ConsoleInfo, SizeOf(TConsoleScreenBufferInfo), 0);
  FillChar(Coord,SizeOf(TCoord),0);
  ConsoleHandle := GetStdHandle(STD_OUTPUT_HANDLE);
  GetConsoleScreenBufferInfo(ConsoleHandle, ConsoleInfo);
  FillConsoleOutputCharacter(ConsoleHandle,' ', ConsoleInfo.dwSize.X * ConsoleInfo.dwSize.Y, Coord, WrittenChars);
  SetConsoleCursorPosition(ConsoleHandle, Coord)
end;

procedure TConsoleApp.ReceiveMessage(AMsg: Integer; AData: RawByteString);
begin
  case AMsg of
    MSGConsole_Key:
      case AData[1] of
        'h':
          showhelp();
        'q':
          quit;
        's':
          FServer.Stop;
        'r':
          FServer.Start;
        'c':
          ClearConsoleWindow;
      end;
  end;
end;

procedure TConsoleApp.Quit;
begin
  LogInfo('terminate');
  PostQuitMessage(0);
end;

procedure TConsoleApp.Run;
var
  vMessage: TMsg;
begin
  while GetMessage(vMessage, 0, 0, 0) do
  begin
    TranslateMessage(vMessage);
    DispatchMessage(vMessage);
  end;
  SetEvent(Event_Close);
end;


procedure TConsoleApp.ShowHelp;
var
  sState: string;
begin
  if FServer.Started  then
    sState:='Started'
  else
    sState:='Stoped';
  Writeln('State:' + sState);
  Writeln('Server Port:' + IntToStr(FServer.Port));
end;

procedure TConsoleApp.ShowStartHelp;
begin
  Writeln(
    'h,F1 - help'#13#10+
    'q - quit'#13#10+
    's - stop'#13#10+
    'r - run'#13#10+
    'c - clear');
end;

procedure TConsoleApp.Start;
begin
  init;
  FServer.Start;
  Run;
end;

function TConsoleApp._AddRef: Integer;
begin
  result := -1
end;

function TConsoleApp._Release: Integer;
begin
  result := -1
end;

end.
