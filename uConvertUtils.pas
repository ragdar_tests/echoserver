unit uConvertUtils;

interface

uses Classes, Types, SysUtils, ansistrings;

// ��������������...
// �� UCS2 � ������
function HexUCS2ToString(HexUCS2Text:ansistring; var sResult: UnicodeString): Boolean;
// �� UCS2 � UTF-16
function HexUCS2ToWidestring(HexUCS2Text: AnsiString): WideString;
//7-�� ������ �������������
function bit7Encode(AText:RawByteString): RawByteString;
//������������� �������� UCS2 ��� 7-�� ������ �� �������
function PPUDecode(AText: AnsiString): UnicodeString;
// �� ������� � ������
function ByteArrayToStr(Bytes: array of byte): ansistring;
// URL ������������
//function URLDecode(const S: ansistring): ansistring;
// URL ����������
function URLEncode(const S: RawByteString): Ansistring;
// URL ����������, ������ � %20
function URLEncodeSimple(const S: RawByteString): Ansistring;
// URL ���������� ����� \ / :
function URLEncodeFileName(const S: ansistring): ansistring;
// �������������� Ansi to Ascii
function AnToAs(const s: ansistring): ansistring;
//������� ���������� �� classes
procedure QuickSort(SortList: PPointerList; L, R: Integer; SCompare: TListSortCompare);
//���������� ������� ����� �����
procedure SortIntArray(IntDynArray: TIntegerDynArray; Asc:Boolean=true);
//�������������� ������ � ������������ � ������ ����� �����
//function AnsiTextToIntDynArray(AText: AnsiString; ADelimeter: AnsiChar; out AResult: TIntegerDynArray): Boolean;

//function ConvertOemToAnsi(const AText: ansistring): ansistring;

// 7-������ �����������
function StrTobit7(buffer8bit:RawByteString): RawByteString;

function Bit7ToStr(buffer7bit: RawByteString): AnsiString;

function HEXbit7ToAnsiStr(HEXbit7:ansistring; var AResult: ansistring): Boolean;

function HexEncode(const S: ansistring): ansistring;

function StrToHexBit7(AText:ansistring):ansistring;

//������������� ����� +CUSD:<m>,<str>,<dcs>
function ParseCUSDRow(const ARow: ansistring): unicodestring;
//������������� ������������� ����� � +CUSD:<m>,<str>,<dcs>
function ParseCUSDText(const s: ansistring): UniCodeString;
//������� ������ �� ������
function CharDelete(s: unicodestring; c: widechar): unicodestring;

function StringToHEX(const AText:ansistring): ansistring;

procedure CommaTextToStrings(AText: AnsiString; ADelimeter: AnsiChar; Strings: TStrings);

function CurrToS(c: Currency): string;

implementation

uses
  Windows, StrUtils;

function bit7Encode(AText:RawByteString): RawByteString;
var
  cx: integer;
  buffer, buffer8bit: RawByteString;
  c, a, b: Byte;
  i, j, k: Integer;
begin
  cx := Length(AText) div 2;
  SetLength(buffer, cx);
  SetLength(buffer8bit, cx*2);
  cx := HexToBin(PAnsiChar(AText), PAnsiChar(buffer), cx);
//  SetLength(buffer, cx*2);
  b := 0;
  j := 0;
  for i:=1 to cx do
  begin
    //����� �� 0,1,...6 ������ OR
    c := Byte(buffer[i]);
    k := (i - 1) mod 7;
    a := (c shl k) and $7f;
    if k>0 then
      a := a or b;

    Inc(j);
    buffer8bit[j] := AnsiChar(a);

    b := c shr (7 - k);

    if k = 6 then
    begin
      Inc(j);
      buffer8bit[j] := AnsiChar(b);
    end;

  end;
  SetLength(buffer8bit,j);
  Result := buffer8bit;
end;

//1 ������ �� UCS2 � UTF-16
function HexToWideChar(Text: PAnsiChar; Buffer: PWideChar; BufSize: Integer): Integer;
var
  i: integer;
  b: PAnsiChar;
  c: AnsiChar;
begin
  b := PAnsiChar(buffer);
  Result := HexToBin(Text, b, BufSize * 2) div 2;
  for i := 0 to Result - 1 do
  begin
    c := b[2 * i];
    b[2 * i] := b[2 * i + 1];
    b[2 * i + 1] := c;
  end;
end;

//function HexUCS2ToWidestring(HexUCS2Text: ansistring): Widestring;
function HexUCS2ToString(HexUCS2Text: ansistring; var sResult: UnicodeString): Boolean;
var
  LenExpect, LenReal: integer;
  wsResult: Widestring;
begin
  LenExpect := Length(HexUCS2Text) div 4;
  SetLength(wsResult, LenExpect);
  LenReal := HexToWideChar(pansichar(HexUCS2Text), PWideChar(wsResult), LenExpect);
  Result := LenReal = LenExpect;
  SetLength(wsResult, LenReal);
  sResult := wsResult;
end;

function HexUCS2ToWidestring(HexUCS2Text: AnsiString): Widestring;
var
  LenExpect, LenReal: integer;
begin
  LenExpect := Length(HexUCS2Text) div 4;
  SetLength(Result, LenExpect);
  LenReal := HexToWideChar(pansichar(HexUCS2Text), PWideChar(Result), LenExpect);
  if LenReal = LenExpect then
    SetLength(Result, LenReal)
  else
    Result := Widestring(HexUCS2Text);
end;

function ByteArrayToStr(Bytes: array of byte): ansistring;
var
  cnt                   : Integer;
begin
  cnt := Length(Bytes);
  SetLength(Result, cnt);
  if cnt > 0 then
    Move(Bytes[0], Result[1], cnt);
end;

// �� ������������

function WebHexToInt(HexChar: Char): Integer;
begin
  Result := Ord(HexChar);

  if HexChar < '0' then
    Inc(Result, 256 - Ord('0'))
  else if (HexChar < 'A') then
    Inc(Result, 0 - Ord('0'))
  else if (HexChar < 'a') then
    Inc(Result, 10 - Ord('A'))
  else
    Inc(Result, 10 - Ord('a'));
end;

// �� ������������, ��� �������

function HexChar(c: AnsiChar): Byte;
begin
  Result := Byte(c);
  case c of
    '0'..'9': dec(Result, Byte('0'));
    'a'..'f': dec(Result, Byte('a') - 10);
    'A'..'F': dec(Result, Byte('A') - 10);
  end;
end;

// �� ������������, ��� �������

function HexByte(p: PAnsiChar): AnsiChar;
begin
  Result := AnsiChar((HexChar(p[0]) shl 4) + HexChar(p[1]));
end;

//function URLDecode(const S: ansistring): ansistring;
//var
//  i, lens               : Integer;
//  c                     : Char;
//  p                     : pchar;
//begin
//  lens := Length(S);
//  SetLength(Result, lens);
//  p := PChar(Result);
//
//  i := 1;
//  while i <= lens do
//  begin
//    c := S[i];
//    if (c = '%') and (i < lens) and (HexToBin(@S[i + 1], p, 1) > 0) then
//      inc(i, 2)
//    else if c = '+' then
//      p^ := ' '
//    else
//      p^ := c;
//    Inc(p);
//    Inc(i);
//  end;
//  SetLength(Result, p - PChar(Result));
//end;

function URLEncodeFileName(const S: ansistring): ansistring;
var
  i, lens               : Integer;
  c                     : AnsiChar;
  p                     : PAnsiChar;
begin
  lens := Length(S);
  SetLength(Result, lens * 3);
  p := PAnsiChar(Result);

  for i := 1 to lens do
  begin
    c := S[i];
    case c of
      //' ': p^ := '+';
      'A'..'Z', 'a'..'z', '0'..'9','_','.','*','-', '/',':', '\' : p^ := c;
    else
      p^ := '%';
      Inc(p);
      BinToHex(@S[i], p, 1);
      Inc(p);
    end;
    Inc(p);
  end;
  SetLength(Result, p - PAnsiChar(Result));
end;

function URLEncodeSimple(const S: RawByteString): Ansistring;
begin
  Result := ansistrings.StringReplace(URLEncode(S),'+','%20',[rfReplaceAll])
end;

function URLEncode(const S: RawByteString): Ansistring;
var
  i, lens               : Integer;
  c                     : AnsiChar;
  p                     : PAnsiChar;
begin
  lens := Length(S);
  SetLength(Result, lens * 3);
  p := PAnsiChar(Result);

  for i := 1 to lens do
  begin
    c := S[i];
    case c of
      ' ': p^ := '+';
      'A'..'Z', 'a'..'z', '0'..'9','_','.','*','-' : p^ := c;
    else
      p^ := '%';
      Inc(p);
      BinToHex(@S[i], p, 1);
      Inc(p);
    end;
    Inc(p);
  end;
  SetLength(Result, p - PAnsiChar(Result));
end;

// �������������� Ansi to Ascii
function AnToAs(const s: ansistring): ansistring;
var
  i, kod, ln            : Integer;
begin
  Result := s;
  ln := Length(s);

  for i := 1 to ln do
  begin
    kod := Ord(s[i]);

    if (kod >= 192) and (kod <= 239) then
      Result[i] := AnsiChar(kod - 64)
    else if (kod >= 240) and (kod <= 255) then
      Result[i] := AnsiChar(kod - 16)
    else if (kod = 168) then
      Result[i] := AnsiChar(240)
    else if (kod = 184) then
      Result[i] := AnsiChar(241)
    else if AnsiChar(kod) = AnsiChar('�') then
      Result[i] := AnsiChar($FC);
  end;
end;

//function ConvertOemToAnsi(const AText: ansistring): ansistring;
//var
//  BufLength: Integer;
//begin
//  BufLength := Length(AText);
//  SetLength(Result, BufLength);
//  OemToCharBuff(PChar(AText), PChar(Result), BufLength);
//end;

procedure QuickSort(SortList: PPointerList; L, R: Integer; SCompare: TListSortCompare);
var
  I, J: Integer;
  P, T: Pointer;
begin
  repeat
    I := L;
    J := R;
    P := SortList^[(L + R) shr 1];
    repeat
      while SCompare(SortList^[I], P) < 0 do
        Inc(I);
      while SCompare(SortList^[J], P) > 0 do
        Dec(J);
      if I <= J then
      begin
        T := SortList^[I];
        SortList^[I] := SortList^[J];
        SortList^[J] := T;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      QuickSort(SortList, L, J, SCompare);
    L := I;
  until I >= R;
end;

function CompareIntegerAsc(Item1, Item2: Integer): Integer;
begin
  if Item1>Item2 then
    Result := 1
  else
  if Item1<Item2 then
    Result := -1
  else
    Result := 0;
end;

function CompareIntegerDesc(Item1, Item2: Integer): Integer;
begin
  if Item1>Item2 then
    Result := -1
  else
  if Item1<Item2 then
    Result := 1
  else
    Result := 0;
end;

procedure SortIntArray(IntDynArray: TIntegerDynArray; Asc:Boolean=true);
var
  CompareFunction: TListSortCompare;
begin
  if Length(IntDynArray)>1 then
  begin
    if Asc then
      CompareFunction := @CompareIntegerAsc
    else
      CompareFunction := @CompareIntegerDesc;
    QuickSort(PPointerList(IntDynArray),0,High(IntDynArray), CompareFunction);
  end;
end;

//function AnsiTextToIntDynArray(AText: AnsiString; ADelimeter: AnsiChar; out AResult: TIntegerDynArray): Boolean;
//var
//  i, lenText, StartPos, ItemCount: Integer;
//  sItem: ansistring;
//begin
//  if Trim(AText) = '' then
//  begin
//    Result := True;
//    SetLength(AResult, 0);
//    Exit;
//  end;
//
//  Result := False;
//  lenText := Length(AText);
//  SetLength(AResult, lenText);
//  StartPos := 1;
//  ItemCount := 0;
//
//  for i := 1 to lenText + 1 do
//  begin
//    if (i > lenText) or (AText[i] = ADelimeter) then
//    begin
//      sItem := Trim(Copy(AText, StartPos, i - StartPos));
//      Result := TryStrToInt(sItem, AResult[ItemCount]);
//      if Result then
//        Inc(ItemCount)
//      else
//        Break;
//      StartPos := i + 1;
//    end;
//  end;
//  SetLength(AResult, ItemCount);
//end;

// 7-������ �����������
function StrTobit7(buffer8bit:RawByteString): RawByteString;
var
  i, j, rgt, lgt: Integer;
  c, a, b: Byte;
  p: PByte;
begin
  if ((Length(buffer8bit) + 1) mod 8) = 0 then
    buffer8bit := buffer8bit + Chr($D);
  p := PByte(buffer8bit);
  SetLength(Result,Length(buffer8bit));
  rgt := (8 - 1) mod 8;//7
  a := 0;
  j := 0;
  for i := 1 to Length(buffer8bit)+1 do
  begin
    c := p^ and $7F; //����������� ������� ��� (��� ������?)
    Inc(p);
    if rgt<7 then
    begin
      lgt := 7 - rgt;
      b := (c shl lgt) and $ff;
      Inc(j);
      Result[j] := AnsiChar(a or b);
    end;
    rgt := (i - 1) mod 8;
    a := c shr rgt;
  end;
  SetLength(Result,j);
end;

function Bit7ToStr(buffer7bit: RawByteString): AnsiString;
var
  cx: integer;
  buffer8bit: RawByteString;
  c, a, b: Byte;
  i, j, k: Integer;
begin
  cx := Length(buffer7bit);
  SetLength(buffer8bit, cx*2);
  b := 0;
  j := 0;
  for i:=1 to cx do
  begin
    //����� �� 0,1,...6 ������ OR
    c := Byte(buffer7bit[i]);
    k := (i - 1) mod 7;
    a := (c shl k) and $7f;
    if k>0 then
      a := a or b;

    Inc(j);
    buffer8bit[j] := AnsiChar(a);

    b := c shr (7 - k);

    if k = 6 then
    begin
      Inc(j);
      buffer8bit[j] := AnsiChar(b);
    end;

  end;
  if (j > 0) and (j mod 8 = 0) and (buffer8bit[j] = Chr($d))  then
    dec(j);
  SetLength(buffer8bit,j);
  Result := buffer8bit;
end;

function HEXbit7ToUStr(HEXbit7: ansistring; var AResult: Unicodestring): Boolean;
var
  AnsiResult: ansistring;
begin
  result := HEXbit7ToAnsiStr(HEXbit7, AnsiResult);
  if result then
    AResult := Unicodestring(AnsiResult)
end;

function HEXbit7ToAnsiStr(HEXbit7: ansistring; var AResult: AnsiString): Boolean;
var
  LenExcpected, LenReal: integer;
  buffer: RawByteString;
begin
  LenExcpected := Length(HEXbit7) div 2;
  SetLength(buffer, LenExcpected);
  LenReal := HexToBin(PAnsiChar(HEXbit7), PAnsiChar(buffer), LenExcpected);
  Result := LenReal = LenExcpected;
  SetLength(buffer, LenReal);
  AResult := Bit7ToStr(buffer);
end;

function PPUDecode(AText: AnsiString): UnicodeString;
var
  a: AnsiString;
  u: UnicodeString;
  Buf: ansistring;
  L: integer;
begin
  u := UnicodeString(AText);
  L := Length(AText) div 2;
  SetLength(Buf,L);
  if L = HexToBin(PAnsiChar(AText), PAnsiChar(Buf), L) then
  begin
//  if pos(' ', AText) = 0 then
    u := CharDelete(HexUCS2ToWidestring(AText),#0);
    if (u = '') or (u[1] = '?') then
      if HEXbit7ToAnsiStr(AText, a) then
        u := UnicodeString(a)
      else
        u := UnicodeString(AText);
  end;

  Result := u;
end;

function HexEncode(const S: ansistring): ansistring;
var
  lens               : Integer;
begin
  lens := Length(S);
  SetLength(Result, lens * 2);
  if lens>0 then
    BinToHex(@S[1], PAnsiChar(Result), lens);
end;

function StrToHexBit7(AText:ansistring):ansistring;
begin
  Result := HexEncode(StrToBit7(AText));
end;

function PosCharR(const Str: ansistring; Chr: AnsiChar): Integer;
begin
  Result := Length(Str);
  while (Result > 0) and (Str[Result] <> Chr) do
    Dec(Result);
end;

function ExtractQuotedString(const s: ansistring): ansistring;
var
  k1, k2: integer;
begin
  // ����������� ������ � ��������� �������
  k1 := pos(ansistring('"'), s);
  k2 := PosCharR(S, '"');
  // ��������� ������ � ��������
  Result := Copy(S, k1 + 1, k2 - k1 - 1);
end;

const
  sCUSD = '+CUSD:';
function Parse_USSD_Response(AResponse: ansistring; var AText: ansistring; var m, dcs: Integer):Boolean;
//Response is +CUSD:<m>,<str>,<dcs>
var
  S: ansistring;
  icomma1, icomma2, k1, k2: Integer;//������� ������� � �������
begin
  S := AResponse;
  result := Pos(ansistring(sCUSD), S)=1;
  if result then
  begin
    Delete(S, 1, Length(sCUSD));
    icomma1 := pos(ansichar(','), S);
    m := StrToIntDef(string(AnsiMidStr(S, 1, icomma1 - 1)),0);
    k1 := Ansistrings.PosEx('"', S, iComma1+1);
    k2 := PosCharR(S, '"');
    AText := Copy(S, k1 + 1, k2 - k1 - 1);
    icomma2 := Ansistrings.posEx(',', S, k2+1);
    if icomma2>0 then
      dcs := StrToIntDef(string(AnsiMidStr(S, icomma2+1,MaxInt)),0)
    else
      dcs := 0;
  end;
end;

function CharDelete(s: unicodestring; c: widechar): unicodestring;
var
  i: Integer;
begin
  Result := s;
  while True do
  begin
    i := Pos(c, Result);
    if i=0 then
      break;
    Delete(Result, i, 1);
  end;
end;

function ParseCUSDRow(const ARow: ansistring): unicodestring;
var
  AnsiText: ansistring;
  m, dcs: Integer;
begin
  if Parse_USSD_Response(ARow, AnsiText, m, dcs) then
    case dcs of
      72:
        Result := CharDelete(HexUCS2ToWidestring(AnsiText), #0);
      1:
        if not HEXbit7ToUStr(AnsiText, result) then
          result := unicodestring(AnsiText);
      //0://15
      //  result := AText;
    else
      if not HEXbit7ToUStr(AnsiText, result) then
        if not HexUCS2ToString(AnsiText, result) then
          result := unicodestring(AnsiText);
    end
  else
    result := unicodestring(ARow);
end;

function ParseCUSDText(const s: ansistring): UniCodeString;
var
  i: Integer;
  slIn, slOut : TStringList;
begin
  slIn := TStringList.Create;
  slOut := TStringList.Create;
  try
    slIn.Text := UniCodeString(s);
    for i:=0 to slIn.count-1 do
      slOut.Add(ParseCUSDRow(ansistring(slIn[i])));
    Result := slOut.Text;
  finally
    FreeAndNil(slIn);
    FreeAndNil(slOut);
  end
end;

function StringToHEX(const AText:ansistring): ansistring;
var
  i:Integer;
begin
  Result:='';
  for i:=1 to Length(AText) do
    Result:= Result + ansistring(IntToHex(Ord(AText[i]),2)) + ansistring(' ');
end;

procedure CommaTextToStrings(AText: AnsiString; ADelimeter: AnsiChar; Strings: TStrings);
var
  i, lenText, StartPos: Integer;
  sItem: string;
begin
  Strings.Clear;
  lenText := Length(AText);
  StartPos := 1;
  for i := 1 to lenText + 1 do
  begin
    if (i > lenText) or (AText[i] = ADelimeter) then
    begin
      sItem := Trim(string(Copy(AText, StartPos, i - StartPos)));
      Strings.Add(sItem);
      StartPos := i + 1;
    end;
  end;
end;

function UCurrToS(i: UInt64): string;
var
  M, D: int64;
begin
  i := i + 50;
  M := i div 10000;
  D := i mod 10000 div 100;
  if D = 0 then
    Result := IntToStr(M)
  else
  if D > 9 then
    Result := IntToStr(M) + '.' + IntToStr(D)
  else
    Result := IntToStr(M) + '.0' + IntToStr(D);
end;

function CurrToS(c: Currency): string;
var
  i: int64;
begin
  i := pInt64(@c)^;
  if i < -49 then
    Result := '-' + UCurrToS(-i)
  else
    Result := UCurrToS(i);
end;

end.


