unit uConsoleThread;

interface
uses
  Messages, Windows, classes, SysUtils, uMQ;
const
  MSGConsole_Key = 0;
  MSGConsole_showhelp = ord('h');
  MSGConsole_quit = ord('q');
  MSGConsole_Stop = ord('s');
  MSGConsole_Start = ord('r');
  MSGConsole_ClearConsoleWindow = ord('c');

type
  TConsoleThread = class(TThread)
  private
    FMQ: IMQ;
  protected
    procedure Execute; override;
  public
//    FMainThread: Cardinal;
    procedure SetMQ(MQ: IMQ);
  end;

implementation

{ TReadlnThread }

procedure TConsoleThread.Execute;
var
  hConsole: THandle;
  Status: DWORD;
  InputBuffer : TInputRecord;
  KeyEvent: TKeyEventRecord;
  Count: DWORD;
  Ch: Char;
begin
  hConsole := GetStdHandle(STD_INPUT_HANDLE);
  while not Terminated do
  begin
    Status := WaitForSingleObject(hConsole, 1000);
    if Status = WAIT_OBJECT_0 then
    begin
      if ReadConsoleInput(hConsole, InputBuffer, 1, Count) then
        if (InputBuffer.EventType = KEY_EVENT) then
        begin
          KeyEvent := InputBuffer.Event.KeyEvent;
          if KeyEvent.bKeyDown then
          begin
            if KeyEvent.wVirtualKeyCode = VK_F1 then
              FMQ.Send(MSGConsole_showhelp, 'F1')
            else
            if KeyEvent.UnicodeChar <> #0 then
            begin
              Ch := KeyEvent.UnicodeChar;
              case Ch of
                'A'..'Z':
                  Ch := Char(Word(Ch) or $0020);
              end;
              FMQ.Send(MSGConsole_Key, RawByteString(Ch));
            end;
//    begin
//      Terminate;
//      FMQ.Send('terminate');
//    end
//    else
//    if (cmd = 's') or (cmd = 'stop') then
//      FMQ.Send('stop')
//    else
//    if (cmd = 'r') or (cmd = 'run') then
//      FMQ.Send('start')
//    else
//      Writeln('unknown command');

//            if KeyEvent.UnicodeChar =  <> #0 then
//              cmd := KeyEvent.UnicodeChar;
//            if KeyEvent.wVirtualKeyCode = VK_F1 then
//              cmd := 'f1'
//            else
//
//                        FMQ.Send(Char(KeyEvent.wVirtualScanCode shl 16));
          end;
        end;
    end;
  end;
//  PostThreadMessage(FMainThread, WM_QUIT, 0, 0);
end;

//procedure TConsoleThread.Execute;
//var
//  cmd: string;
//begin
//  while not Terminated do
//  begin
//    Write('>');
//    Readln(cmd);
//    cmd := lowercase(Cmd);
//    if (cmd = 'q') or (cmd = 'quit') or (cmd = 'exit') then
//    begin
//      Terminate;
//      FMQ.Send('terminate');
//    end
//    else
//    if (cmd = 's') or (cmd = 'stop') then
//      FMQ.Send('stop')
//    else
//    if (cmd = 'r') or (cmd = 'run') then
//      FMQ.Send('start')
//    else
//      Writeln('unknown command');
//  end;
//  PostThreadMessage(FMainThread, WM_QUIT, 0, 0);
////  WaitForSingleObject(FMainThread, 1000);
////  PostQuitMessage(0);
//end;

procedure TConsoleThread.SetMQ(MQ: IMQ);
begin
  FMQ := MQ;
end;

end.
