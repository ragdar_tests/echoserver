﻿program EchoServer;

{$APPTYPE CONSOLE}

{$R *.res}
{$DEFINE FullDebugMode}
{$DEFINE LogMemoryLeakDetailToFile}

uses
  System.SysUtils,
  uConsoleApp in 'uConsoleApp.pas',
  uConsoleThread in 'uConsoleThread.pas',
  uMQ in 'uMQ.pas',
  uServer in 'uServer.pas',
  uDateUtils in 'uDateUtils.pas',
  uQueue in 'uQueue.pas',
  uAppUtils in 'uAppUtils.pas',
  uLogging in 'log\uLogging.pas',
  uLogintf in 'log\uLogintf.pas',
  uLogObj in 'log\uLogObj.pas',
  uLogRoot in 'log\uLogRoot.pas',
  uLogWriterFile in 'log\uLogWriterFile.pas';
  
procedure main;
var
  ConsoleApp: TConsoleApp;
  Logging: ILogging;
  RootLog: ILog;
begin
  ReportMemoryLeaksOnShutdown := True;
  Logging := TLogging.Create(GetLogPath);
  RootLog := Logging.GetLog('');
  RootLog.Log(lmInfo,'=== start ===');
  try
    ConsoleApp := TConsoleApp.Create(RootLog);
    try
      ConsoleApp.Start;
    finally
      ConsoleApp.Free;
    end;
  except
    on E: Exception do
      RootLog.Log(lmCritical, E.ClassName + ': ' + E.Message);
  end;
  RootLog.Log(lmInfo,'=== stop ===');
end;

begin
  try
    main;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

