unit uQueue;

interface
uses
  SysUtils, Classes;

type

  IInterfaceQueue = interface
    function PushItem(AIndex: Integer; Item: IInterface): Boolean;
    function PopItem(AIndex: Integer; var Item: IInterface): Boolean;
    function PushFirst(Item: IInterface): Integer;
    function PopFirst(var Item: IInterface): Boolean;
    function PushLast(Item: IInterface): Integer;
    function PopLast(var Item: IInterface): Boolean;
    function GetCount(): Integer;
    procedure Clear;
    function Get(Index: Integer): IInterface;
    function IndexOf(const Item: IInterface): Integer;
    function First: IInterface;
    function Last: IInterface;
  end;

  TInterfaceQueue = class(TInterfacedObject, IInterfaceQueue)
  private
    FList: TInterfaceList;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    { IInterfaceQueue }
    function PushItem(AIndex: Integer; Item: IInterface): Boolean;
    function PopItem(AIndex: Integer; var Item: IInterface): Boolean;
    function PushFirst(Item: IInterface): Integer;
    function PopFirst(var Item: IInterface): Boolean;
    function PushLast(Item: IInterface): Integer;
    function PopLast(var Item: IInterface): Boolean;
    function GetCount(): Integer;
    procedure Clear;
    function Get(Index: Integer): IInterface;
    function IndexOf(const Item: IInterface): Integer;
    function First: IInterface;
    function Last: IInterface;
//
//    function PushItem(AIndex: Integer; Item: IUnknown): Boolean;
//    function PopItem(AIndex: Integer; var Item: IUnknown): Boolean;
//    procedure PushFirst(Item: IUnknown);
//    function PushLast(Item: IUnknown): Integer;
//    function PopFirst(var Item: IUnknown): Boolean;
//    function PopLast(var Item: IUnknown): Boolean;
//    function GetCount(): Integer;
  end;

implementation

{ TInterfaceQueue }
function TInterfaceQueue.PopItem(AIndex: Integer; var Item: IUnknown): Boolean;
begin
  if AIndex = -1 then
    AIndex := FList.count - 1;
  Result := (0 <= AIndex) and (AIndex < FList.count);
  if Result then
  begin
    Item :=  FList[AIndex];
    FList.Delete(AIndex);
  end;
end;

function TInterfaceQueue.PopFirst(var Item: IUnknown): Boolean;
begin
  Result := PopItem(0, Item);
end;

function TInterfaceQueue.PopLast(var Item: IUnknown): Boolean;
begin
  Result := PopItem(FList.Count - 1, Item);
end;

function TInterfaceQueue.PushFirst(Item: IUnknown): Integer;
begin
  FList.Insert(0, Item);
  Result := 0;
end;

function TInterfaceQueue.PushLast(Item: IUnknown): Integer;
begin
  Result := FList.Add(Item);
end;

constructor TInterfaceQueue.Create;
begin
  FList := TInterfaceList.Create;
end;

function TInterfaceQueue.PushItem(AIndex: Integer; Item: IUnknown): Boolean;
begin
  if AIndex = -1 then
    AIndex := FList.Count -1;
  Result := (0 <= AIndex) and (AIndex <= FList.Count);
  if Result then
    FList.Insert(AIndex, Item);
end;

destructor TInterfaceQueue.Destroy;
begin
  FreeAndNil(FList);
  inherited;
end;

function TInterfaceQueue.GetCount: Integer;
begin
  Result := FList.Count;
end;

procedure TInterfaceQueue.Clear;
begin
  FList.Clear;
end;

function TInterfaceQueue.First: IInterface;
begin
  FList.First;
end;

function TInterfaceQueue.Get(Index: Integer): IInterface;
begin
  Result := FList.Items[index];
end;

function TInterfaceQueue.IndexOf(const Item: IInterface): Integer;
begin
  Result := FList.IndexOf(Item);
end;

function TInterfaceQueue.Last: IInterface;
begin
  FList.Last;
end;

end.

