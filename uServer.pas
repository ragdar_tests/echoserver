unit uServer;

interface

uses
  Windows, Messages, SysUtils, Classes, StrUtils,
  uLogIntf, uAppUtils, OverbyteIcsWSocket, OverbyteIcsHttpSrv;

const
  NO_CACHE = 'Pragma: no-cache' + #13#10 + 'Expires: -1' + #13#10;
//  WM_CLIENT_COUNT = WM_USER + 1;
  MAX_UPLOAD_SIZE = 8 * 1024 * 1024;
type

  TServer = class
  private
    FLog: ILog;
    FHttpServer: THttpServer;
    procedure HttpServerPostedData(Sender: TObject; Client: TObject; Error: Word);
    procedure HttpServerPostDocument(Sender, Client: TObject; var Flags: THttpGetFlag);
    procedure HttpServerGetDocument(Sender, Client: TObject; var Flags: THttpGetFlag);
    procedure CreateDocument_Get(Sender: TObject; ClientCnx : THttpConnection;
       var Flags : THttpGetFlag; Path: string);
    procedure CreateDocument_Post(Sender: TObject; ClientCnx : THttpConnection;
       Response: ansistring);
//    function GetJson(): UTF8String;
    function GetStarted: Boolean;
    procedure SetStarted(const Value: Boolean);
    procedure SetPort(const Value: Integer);
    function GetPort(): Integer;
    procedure LogResponse(Stream: TStream);
    procedure LogInfo(const AMSG: string);
    //procedure LogError(const AMSG: string);
  public
    constructor Create(ALog: ILog);
    destructor Destroy; override;
    procedure Stop();
    procedure Start();
    procedure DisplayHeader(ClientCnx : THttpConnection);
    property HttpServer: THttpServer read FHttpServer;
    property Started: Boolean read GetStarted write SetStarted;
    property Port: Integer read GetPort write SetPort;
  end;

implementation

procedure TServer.DisplayHeader(ClientCnx: THttpConnection);
begin
  FLog.Log(lmDebug, 'headers:'#13#10+ ClientCnx.RequestHeader.Text);
end;

function TServer.GetStarted: Boolean;
begin
  Result := FHttpServer.WSocketServer.State = wsListening;
end;

procedure TServer.SetPort(const Value: Integer);
begin
  FHttpServer.Port := IntToStr(Value);
end;

function TServer.GetPort(): Integer;
begin
  Result := StrToIntDef(FHttpServer.Port, 0);
end;

procedure TServer.SetStarted(const Value: Boolean);
begin
  if  Value then
    FHttpServer.Start
  else
    FHttpServer.Stop
end;

procedure TServer.Start();
begin
  FHttpServer.Start;
  LogInfo('Start Server on Port ' + FHttpServer.Port);
end;

procedure TServer.Stop();
begin
  FHttpServer.Stop;
  LogInfo('Stop');
end;

procedure TServer.HttpServerGetDocument(Sender, Client: TObject; var Flags: THttpGetFlag);
var
  ClientCnx: THttpConnection;
  Params, PeerAddr, Version, Request: string;
begin
  if Flags = hg401 then
      Exit;
  { It's easyer to do the cast one time. Could use with clause... }
  ClientCnx := THttpConnection(Client);
  { Count request and display a message }
  if ClientCnx.Params <>'' then
    Params := '?' + ClientCnx.Params;
  PeerAddr := 'PeerAddr: '+ ClientCnx.GetPeerAddr;
  Version := 'Version: ' + ClientCnx.Version;
  Request := 'Request: GET ' + ClientCnx.Path + Params;
  FLog.Log(lmTrace, PeerAddr + ', '+ Version + ', ' + Request);
  DisplayHeader(ClientCnx);

//  if CompareText(ClientCnx.Path, '/get') = 0 then
//    CreateVirtualDocument_Get(Sender, ClientCnx, Flags)
//  else
  ClientCnx.RequestHeader.Add(Version);
  ClientCnx.RequestHeader.Add(Request);
  CreateDocument_Get(Sender, ClientCnx, Flags, ClientCnx.RequestHeader.Text);
//  if CompareText(ClientCnx.Path, '/post') = 0 then
//    CreateVirtualDocument_Post(Sender, ClientCnx, Flags)
//  else
//    CreateVirtualDocument_Any(Sender, ClientCnx, Flags);
end;

constructor TServer.Create(ALog: ILog);
begin
  FLog := ALog.GetChild('server');
  FHttpServer := THttpServer.Create(nil);
  FHttpServer.OnGetDocument := HttpServerGetDocument;
  FHttpServer.OnPostedData :=  HttpServerPostedData;
  FHttpServer.OnPostDocument :=  HttpServerPostDocument;
end;

destructor TServer.Destroy;
begin
  FHttpServer.Free;
  inherited;
end;

procedure TServer.LogInfo(const AMSG: string);
begin
  Writeln(AMSG);
  FLog.Log(lmInfo, AMSG)
end;

procedure TServer.LogResponse(Stream: TStream);
var
  Response: UTF8String;
begin
//  Stream.Seek(0,0);
  SetLength(Response, Stream.Size);
  Stream.Read(Response[1], Length(Response));
  FLog.Log(lmInfo, 'Response:'#13#10 + string(Response));
  Stream.Seek(0,0);
end;

procedure ReloadFromFile(FileName: string; var Response: ansistring);
var
  Stream: TStream;
begin
  if FileExists(FileName) then
  begin
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      SetLength(Response, Stream.Size);
      Stream.Read(Pointer(Response)^, Stream.Size);
    finally
      Stream.Free;
    end;
  end;
end;


procedure TServer.CreateDocument_Post(Sender: TObject; ClientCnx : THttpConnection; Response: ansistring);
var
  Dummy: THttpGetFlag;
  Params, PeerAddr, Version, Request: string;
  sl: TStringList;
begin
  sl := TStringList.Create;
  sl.Text := ClientCnx.RequestHeader.Text;
  if ClientCnx.Params <>'' then
    Params := '?' + ClientCnx.Params;
  PeerAddr := 'PeerAddr: '+ ClientCnx.GetPeerAddr;
  Version := 'Version: ' + ClientCnx.Version;
  Request := 'Request: POST ' + ClientCnx.Path + Params;

  sl.Add(Version);
  sl.Add(Request);
//  Stream := TBytesStream.Create(BytesOf(GetJSONPays(0,Pays)));
  Response := AnsiString(sl.Text) + #10'==='#10 + Response;
  sl.Free;

  ClientCnx.DocStream.Free;

  ReloadFromFile('Response.txt', Response);

  ClientCnx.DocStream := TBytesStream.Create(BytesOf(Response));
  LogResponse(ClientCnx.DocStream);
  //TStringStream.Create(GetAppTitle + '.htm', fmOpenRead);
  ClientCnx.AnswerStream(Dummy,
    '', { Default Status '200 OK' }
    '', { Default Content-Type: text/html }
    '');// { Default header  }
//    GetJson,
//    CP_UTF8);
end;

procedure TServer.CreateDocument_Get(
    Sender    : TObject;
    ClientCnx : THttpConnection;
    var Flags : THttpGetFlag;
    Path: string);
var
  Response: AnsiString;
begin
  ClientCnx.DocStream.Free;
  Response := AnsiString(Path);
  ReloadFromFile('Response.txt', Response);
  ClientCnx.DocStream := TBytesStream.Create(BytesOf(Response));
//  TFileStream.Create(GetCfgSysPath + 'ModemProcessing.htm', fmOpenRead);
  ClientCnx.AnswerStream(Flags,
    '',           { Default Status '200 OK'         }
    '',     // text/json      { Default Content-Type: text/html }
    NO_CACHE)           { Default header                  }
end;

procedure TServer.HttpServerPostDocument(
    Sender    : TObject;            { HTTP server component                 }
    Client    : TObject;            { Client connection issuing command     }
    var Flags : THttpGetFlag);      { Tells what HTTP server has to do next }
var
    ClientCnx  : THttpConnection;
begin
  FLog.Log(lmDebug,'=== PostDocument ===');
  { It's easyer to do the cast one time. Could use with clause... }
  ClientCnx := Client as THttpConnection;
  DisplayHeader(ClientCnx);

  if (ClientCnx.RequestContentLength > MAX_UPLOAD_SIZE) or
    (ClientCnx.RequestContentLength <= 0) then
  begin
    FLog.Log(lmInfo,'Upload size exceeded limit (' + IntToStr(MAX_UPLOAD_SIZE) + ')');
    Flags := hg403;
    Exit;
  end;

  Flags := hgAcceptData;
  ClientCnx.LineMode := FALSE;
end;

procedure TServer.HttpServerPostedData(
    Sender : TObject;               { HTTP server component                 }
    Client : TObject;               { Client posting data                   }
    Error  : Word);                 { Error in data receiving               }
var
  ClientCnx  : THttpConnection;
//  Buffer: TByteDynArray;
//  sd: TWSocketData;
  Request: ansistring;
  L: Int64;
begin
  ClientCnx := THttpConnection(Client);
//  FLog.Log(lmDebug, '=== PostedData ===');
  L := ClientCnx.RequestContentLength;
  SetLength(Request, L);
  ClientCnx.Receive(Pointer(Request), L);
  FLog.Log(lmInfo, 'Request:'#13#10 + string(Request));
  CreateDocument_Post(Sender, ClientCnx, Request);
end;

end.

