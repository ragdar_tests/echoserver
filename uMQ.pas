unit uMQ;

interface
uses
  Windows, Messages, System.Generics.Collections, classes;
const
   WM_NEW_MESSAGE = WM_USER + 5624;
type
  // ���������� ���������
  IReceiver = interface
    procedure ReceiveMessage(AMsg: Integer; AData: RawByteString);
  end;

//      procedure AddReceiver(IReceiver);

//  IMQSubsribe = interface
//    procedure AddReceiver(Receiver: IReceiver);
//  end;

  IMQ = interface
    procedure SendMessages();
    procedure Send(AMsg: Integer; AData: RawByteString);
//    procedure SendData(var msg; bytecount: integer);
    procedure Subscribe(Receiver: IReceiver);
    procedure Unsubscribe(Receiver: IReceiver);
  end;


  TRawByteMessage = record
    msg: Integer;
    Data: RawByteString
  end;

  TMQ = class(TInterfacedObject, IMQ)
  private
    FCriticalSection: TRTLCriticalSection;
    FHandle: THandle;//
    FMessages: TQueue<TRawByteMessage>;
//    FName: string;//��� �������
    FReceivers: TList<Pointer>; //Callback ��������� ��� ����������?
//    FLastReceiver: IReceiver;
    procedure WndProc(var Message: TMessage);
    procedure Send(AMsg: Integer; AData: RawByteString);
    procedure Subscribe(Receiver: IReceiver);
    procedure Unsubscribe(Receiver: IReceiver);
    procedure WmNewMessage(var Msg: TMessage); message WM_NEW_MESSAGE;
    procedure SendAll(ARawByteMessage: TRawByteMessage);
  public
    constructor Create();
    destructor Destroy(); override;
    procedure SendMessages();
  end;

implementation

{ TMQ }

procedure TMQ.Subscribe(Receiver: IReceiver);
begin
  EnterCriticalSection(FCriticalSection);
  FReceivers.Add(Pointer(Receiver));
  LeaveCriticalSection(FCriticalSection);
end;

procedure TMQ.Unsubscribe(Receiver: IReceiver);
begin
  EnterCriticalSection(FCriticalSection);
  FReceivers.Remove(Pointer(Receiver));
  LeaveCriticalSection(FCriticalSection);
end;

constructor TMQ.Create;
begin
   FHandle := AllocateHWnd(WndProc);
   FMessages := TQueue<TRawByteMessage>.create;
   FReceivers := TList<Pointer>.Create;
   InitializeCriticalSection(FCriticalSection);
end;

destructor TMQ.Destroy;
//var
//  i: Integer;
begin
  FReceivers.Free;
  FMessages.Free;
  if FHandle <> 0 then
  	DeallocateHWnd(FHandle);
  DeleteCriticalSection(FCriticalSection);
//  for I := FReceivers.Count -1 downto 0 do
//  try
//    IReceiver(FReceivers[i])._Release;
//    FReceivers.Delete(i);
//  except
//    ;; //���������� ��� ������� ���������
//  end;
  inherited;
end;

procedure TMQ.Send(AMsg: Integer; AData: RawByteString);
var
  RawByteMessage: TRawByteMessage;
begin
  EnterCriticalSection(FCriticalSection);
  try
    RawByteMessage.msg := AMsg;
    RawByteMessage.Data := AData + '';
    FMessages.Enqueue(RawByteMessage);
  finally
    LeaveCriticalSection(FCriticalSection);
  end;
  PostMessage(FHandle, WM_NEW_MESSAGE, 0, 0);
end;

procedure TMQ.SendAll(ARawByteMessage: TRawByteMessage);
var
  i: integer;
begin
  for I := 0 to FReceivers.Count -1 do
  try
    IReceiver(FReceivers[i]).ReceiveMessage(ARawByteMessage.msg, ARawByteMessage.Data);
  except
    ;; //���������� ��� ������� ���������
  end;
end;

procedure TMQ.SendMessages();
var
  Msg: TRawByteMessage;
  i: Integer;
//  Exist: Boolean;
begin
  for i := 0 to FMessages.Count - 1 do
  begin
//    Exist := False;
    EnterCriticalSection(FCriticalSection);
    try
//      Exist := FMessages.Count > 0;
//      if Exist then
        Msg := FMessages.Extract;
    finally
      LeaveCriticalSection(FCriticalSection);
    end;
//    if Exist then
      SendAll(Msg)
//    else
//      Break;
  end;
end;

//procedure TMQ.SendMessages1();
//begin
//  EnterCriticalSection(FCriticalSection);
//  try
//    while FMessages.Count > 0 do
//      SendAll(FMessages.Extract);
//  finally
//    LeaveCriticalSection(FCriticalSection);
//  end;
//end;

procedure TMQ.WmNewMessage(var Msg: TMessage);
//var
//  i: Integer;
//  RawByteMessage: TRawByteMessage;
begin
  SendMessages();
////    for i := 0 to FMessages.Count - 1 do
////      SendAll(FMessages.Extract [i]);
////    while FMessages.Count > 0 do
////    begin
////      SendAll(FMessages[0]);
////      FMessages.Delete(0);
////    end;
////    FMessages.Clear;
//  finally
//    LeaveCriticalSection(FCriticalSection);
//  end;
end;

procedure TMQ.WndProc(var Message: TMessage);
begin
  Dispatch(Message);
end;

end.
